#!/bin/bash

TESTING=$1

. `dirname $0`/ssactive
ssactive
ACTIVE=$?

if [ $ACTIVE -eq 0 ] && [ ! $TESTING ]; then
	exit
fi

BANSHEE_PID=`pidof -s banshee`
BANSHEE_CMD='banshee --pause'

PIANOBAR_PID=`pidof -s pianobar`
PIANOBAR_CMD='/home/crthomas/bin/pianobar_controller pause'

if [ $BANSHEE_PID ]; then
	. `dirname $0`/get_dbus
	get_dbus
	SUCCESS=$?

	if [ -z $SUCCESS ]; then
		exit
	fi

	DBUS=`cat /tmp/dbus`
	export DBUS_SESSION_BUS_ADDRESS=$DBUS

	if [ $TESTING ]; then
		echo "export DBUS_SESSION_BUS_ADDRESS=$DBUS"
	else
		$BANSHEE_CMD
	fi
fi

if [ $PIANOBAR_PID ]; then
	$PIANOBAR_CMD
fi
